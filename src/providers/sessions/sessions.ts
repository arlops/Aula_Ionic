import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { App } from 'ionic-angular';

/*
  Generated class for the SessionsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SessionsProvider {

  username = "teste@email.com"
  password = "123456"

  constructor(public app: App) {

  }
login(username: string, password: string): boolean {
  if (username === this.username && password === this.password){
    localStorage.setItem("logado", "true");
    return true;
  }else {
    return false;
  }
}

logout(): void {
  localStorage.removeItem("logado");
}

}
