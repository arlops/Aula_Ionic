import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { SessionsProvider } from '../../providers/sessions/sessions';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public sessions: SessionsProvider, public app: App) {

  }

  logout(): void {
    this.sessions.logout();
    this.app.getRootNav().push(LoginPage);
  }

}
